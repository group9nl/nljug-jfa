CREATE TABLE material
(
    id          int NOT NULL
        CONSTRAINT material_pk
            PRIMARY KEY,
    type        varchar(256),
    designation varchar(256)
);

ALTER TABLE material
    OWNER TO db;

CREATE UNIQUE INDEX material_id_uindex
    ON material (id);

INSERT INTO material (id, type, designation)
SELECT x.id, 'VIRM', '4' || x.id
FROM generate_series(1, 2000000) AS x(id);