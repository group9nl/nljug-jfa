# Java flight recorder examples

This project contains examples used in the NLJUG Java Flight Recorder webcast.

You can view the webcast [here](https://nljug.org/nljug-academy-masterclass-livestream-java-flight-recorder/)

## Perquisites
- Java 14
  - This example project was tested with AdoptOpenJDK 14
- Docker

## Running

### Start the docker containers first. 
```shell
docker compose up
```

The [docker-compose.yml](docker-compose.yml) contains 3 services.

- postgresql
    - A PostgreSQL database. On start, the database is initialized with a single table 'material'. This table is filled with 2.000.000 records.
- influxdb
    - A InfluxDB database we to stream the custom RestCallEvent JFR events to. 
- grafana
    - A Grafana dashboard to display the RestCallEvents that are streamed to the InfluxDB.

### Start the web service using Gradle
```shell
gradle quarkusdev
```

Two endpoints will be available.

- [localhost:8080/checkMaterials](http://localhost:8080/checkMaterials)
    - Retrieves a large set of 'Materials' from the database and checks performs a (very inefficient) check on the retrieved data. Response times will be very slow!
- [localhost:8080/material/{id}](http://localhost:8080/material/1)
    - Retrieves a single 'Material' from the database.

## Creating flight recordings
### From start
Using JVM options, you can enable JFR when starting a Java application. 

Compile the web service:
```shell
./gradlew quarkusBuild
```

Run the jar with flight recorder enabled:
```shell
java -XX:StartFlightRecording=filename=dump.jfr,maxsize=512m -jar build/quarkus-app/quarkus-run.jar
```

This will start a continuous (infinite) recording with a maximum file size of 512mb. When the maximum file size is exceeded, older events will be deleted. It is also possible to specify a maximum age for events using `maxage` e.g:
```shell
java -XX:StartFlightRecording=filename=dump.jfr,maxage=1h -jar build/quarkus-app/quarkus-run.jar
```

It is also possible to start a recording for a fixed duration using the `duration` e.g:
```shell
java -XX:StartFlightRecording=filename=dump.jfr,duration=60s -jar build/quarkus-app/quarkus-run.jar
```

Since we have used the `filename` option, the recording will be written to `dump.jfr` when the JVM exits. You can also create a dump while the flight recording is still running:
```shell
jcmd [id of the Java application] JFR.dump name=1 filename=intermediate_dump.jfr
```

### From command line
You can use the `jcmd` tool to start a flight recording of an already running application using the command line.

To list all running Java applications and their id's use:
```shell
jcmd 
```

To start a flight recording named `rec` of a Java application with id `1`:
```shell
jcmd 1 JFR.start name=rec
```

To see with flight recordings are currently running:
```shell
jcmd 1 JFR.check
```

To create a dump of the flight recording:
```shell
jcmd 1 JFR.dump name=rec filename=rec.jfr
```

To stop the flight recording:
```shell
jcmd 1 JFR.stop name=rec
```
### Using Mission Control
In Mission Control, all running Java applications are displayed in the _JVM Browser_ window. In this view you can click on the arrow next to an application, subsequently click on the arrow next to _Flight Recorder_ to show all flight recordings for this application. 

You can start a new flight recording by double-clicking _Flight Recorder_.

To create a dump of a running flight recording, double-click it.

## Custom events
Starting from JDK 9, it is possible to create your own Flight Recoder events. Using custom events, you can collect data that is specifically tailored to the functionality and domain of your application. 

We have implemented a custom event for our web application: RestCallEvent. Using this event we can monitor the duration and client address for each invocation of one of the two endpoints of our webservice.

See [RestCallEvent.java](src/main/java/nl/ns/jfr/event/RestCallEvent.java) for the implementation of our custom event. In [RestCallFilter.java](src/main/java/nl/ns/jfr/rest/RestCallFilter.java) we submit our event to Flight Recorder.

## JFR Event Streaming
Since JDK 14, Flight recorder comes with Event Streaming. To demonstrate this, we stream our custom RestCallEvents to InfluxDB so that we can display them realtime on a Grafana dashboard. We have included a preconfigured InfluxDB and Grafana services in the [docker-compose.yml](docker-compose.yml).

See [JFRInfluxStreamer.java](src/main/java/nl/ns/jfr/streaming/JFRInfluxStreamer.java) for our example of using JFR Streaming to send our CustomEvents into an InfluxDB server.

Please note that this use of the JFR Streaming comes with a performance overhead that is significantly larger than when we would analyse our events using Flight Recorder dumps. For this reason, we are only streaming our RestCallEvents.

Navigate to [localhost:3000](http://localhost:3000/d/PKJ_tiznk/webservice) to view the Grafana dashboard. You can create a couple of RestCallEvents using:
```shell
for i in {1..100};do curl localhost:8080/material/1;done
```
