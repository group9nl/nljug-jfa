import kotlin.time.milliseconds

plugins {
    id("io.quarkus") version "1.13.6.Final"
    id("java")
    id("application")
}

repositories {
    mavenCentral()
    jcenter()
}

tasks.withType<JavaExec>().configureEach {
    main = "nl.ns.jfr.Main"


    val rootPath = this.project.rootProject.rootDir
    jvmArgs("-Xmx512m", "-XX:StartFlightRecording=dumponexit=true,settings=${rootPath}/JFRConfig.jfc,filename=/tmp/jfrdump-${getTime()}.jfr,path-to-gc-roots=true")
}

tasks.withType<Jar>().configureEach {
    manifest.attributes["Main-Class"] = "nl.ns.jfr.Main"
}


dependencies {
    implementation(enforcedPlatform("io.quarkus:quarkus-universe-bom:1.13.6.Final"))
    implementation("io.quarkus:quarkus-resteasy:1.13.6.Final")
    implementation("io.quarkus:quarkus-hibernate-orm:1.13.6.Final")
    implementation("io.quarkus:quarkus-jdbc-postgresql:1.13.6.Final")
    implementation("com.influxdb:influxdb-client-java:2.2.0")
    implementation("org.slf4j:slf4j-api:2.0.0-alpha1")
}

group = "nl.ns.webcast"
version = "1.0-SNAPSHOT"

@OptIn(kotlin.time.ExperimentalTime::class)
fun getTime(): String {
    return System.currentTimeMillis().milliseconds.toIsoString()
}