package nl.ns.jfr.rest;

import java.util.Collection;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import nl.ns.jfr.data.Material;
import nl.ns.jfr.data.MaterialService;
import nl.ns.jfr.data.MaterialVerfier;

@Path("/")
public class RestEndpoint {

    private final MaterialVerfier materialVerfier = new MaterialVerfier();
    @Inject
    MaterialService materialService;

    @Path("material/{id}")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Material getById(@PathParam("id") int id) {

        return materialService.getMaterial(id);
    }

    @Path("checkMaterials")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public boolean contains() {
        final Collection<Material> materials = materialService.getMaterial();
        return materialVerfier.checkNumbers(materials);
    }
}
