package nl.ns.jfr.rest;

import io.vertx.core.http.HttpServerRequest;
import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import nl.ns.jfr.event.RestCallEvent;

@Provider
public class RestCallFilter implements ContainerRequestFilter, ContainerResponseFilter {


    public static final String EVENT = "EVENT";
    @Context
    HttpServerRequest httpServerRequest;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        final RestCallEvent restCallEvent = new RestCallEvent();
        restCallEvent.begin();

        final String path = requestContext.getUriInfo().getPath();
        final String clientAddress = httpServerRequest.remoteAddress().host();

        restCallEvent.setEndpoint(path);
        restCallEvent.setIp(clientAddress);

        requestContext.setProperty(EVENT, restCallEvent);
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        final RestCallEvent event = (RestCallEvent) requestContext.getProperty(EVENT);

        if (event != null) {
            event.commit();
        }
    }
}
