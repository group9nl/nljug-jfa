package nl.ns.jfr.streaming;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.write.Point;
import io.quarkus.runtime.Startup;
import java.time.temporal.ChronoUnit;
import javax.enterprise.context.ApplicationScoped;
import jdk.jfr.consumer.RecordingStream;
import nl.ns.jfr.event.RestCallEvent;

@Startup
@ApplicationScoped
public class JFRInfluxStreamer {

    private final InfluxDBClient influxDBClient = InfluxDBClientFactory.create(
            "http://localhost:8086", "token".toCharArray(), "NS", "JFR"
    );

    public JFRInfluxStreamer() {
        final RecordingStream recordingStream = new RecordingStream();
        recordingStream.enable(RestCallEvent.class);

        recordingStream.onEvent(RestCallEvent.class.getName(), recordedEvent -> {
            influxDBClient.getWriteApi().writePoint(
                    Point.measurement("RestCallEvent")
                         .addField("ip", recordedEvent.getValue("ip").toString())
                         .addField("endpoint", recordedEvent.getValue("endpoint").toString())
                         .addField("duration", recordedEvent.getDuration().get(ChronoUnit.NANOS))
            );
        });

        recordingStream.startAsync();
    }
}
