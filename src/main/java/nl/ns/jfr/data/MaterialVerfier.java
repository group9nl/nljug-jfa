package nl.ns.jfr.data;

import java.util.Collection;
import java.util.stream.IntStream;

public class MaterialVerfier {

    public static final int YIELD_EACH = 5_000;

    public static final int CHECKS = 7;
    public static final int THREAD_COUNT = 16;

    public MaterialVerfier() {
    }

    public boolean checkNumbers(Collection<Material> materials) {

        final Runnable runnable = () -> {
            for (int i = 0; i < CHECKS; i++) {
                materials.forEach(material -> {
                    boolean check = material.getId() <= materials.size();
                    if (!check) {
                        throw new RuntimeException("weird");
                    }
                });
            }
        };

        final ThreadGroup restWorkers = new ThreadGroup("rest workers");
        IntStream.range(0, THREAD_COUNT)
                 .mapToObj(value -> {
                     final Thread thread = new Thread(restWorkers, runnable);
                     thread.start();
                     return thread;
                 })
                 .forEach(thread -> {
                     try {
                         thread.join();
                     } catch (InterruptedException e) {
                         e.printStackTrace();
                     }
                 });

        return true;
    }
}