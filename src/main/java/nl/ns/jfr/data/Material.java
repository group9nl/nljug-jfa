package nl.ns.jfr.data;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Material {

    private int id;

    private String type;

    private String designation;


    public Material() {

    }

    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Material{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", designation='" + designation + '\'' +
                '}';
    }
}
