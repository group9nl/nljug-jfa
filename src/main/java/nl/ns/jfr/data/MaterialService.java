package nl.ns.jfr.data;

import java.util.Collection;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import org.hibernate.CacheMode;
import org.hibernate.jpa.QueryHints;

@ApplicationScoped
public class MaterialService {
    @Inject
    EntityManager entityManager;


    @Transactional
    public Collection<Material> getMaterial() {
        final TypedQuery<Material> query = entityManager.createQuery("select m from Material m", Material.class);
        query.setHint(QueryHints.HINT_CACHE_MODE, CacheMode.IGNORE);
        return query.getResultList();
    }

    public Material getMaterial(int id) {
        final TypedQuery<Material> query = entityManager.createQuery("select m from Material m where m.id = :id", Material.class).setParameter("id",id);

        return query.getSingleResult();
    }
}
